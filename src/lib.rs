use ansi_term::Colour::{Green, White};

pub mod games;

pub fn run(game: &str) {
    match game {
        "guess" => games::guess::run(),
        "fizzbuzz" => games::fizzbuzz::run(),
        "help" => games::help::run(""),
        _ => games::help::run(game),
    }
}

fn title(s: &str) {
    println!(
        "{}",
        White.bold().underline().on(Green).paint(format!(" {} ", s))
    );
    println!();
}

fn end() {
    println!();
    println!("{}", White.on(Green).bold().underline().paint(" THE END "));
}
