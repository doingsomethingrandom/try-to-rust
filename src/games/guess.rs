use ansi_term::Colour::{Cyan, Green, Red, White};
use rand::distributions::{Distribution, Uniform};
use inquire::Text;
use std::cmp::Ordering;

pub fn run() {
    crate::title("GUESS");
    println!(
        "{}",
        Cyan.underline()
            .paint("HINT: The number is between 1 and 10")
    );
    let mut rng = rand::thread_rng();
    let rand_num = Uniform::from(1..11).sample(&mut rng);
    loop {
        let guess = match Text::new("Enter your guess:").prompt() {
            Ok(str) => str,
            Err(_) => panic!("{}", Red.blink().paint("Could not read input!"))
        };
        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => panic!("{}", Red.blink().paint("You must in put a number!")),
        };

        println!("{}", White.bold().paint(format!("You guessed {}.", guess)));

        match guess.cmp(&rand_num) {
            Ordering::Less => println!(
                "{}",
                Red.bold()
                    .paint("Too small! Try again")
            ),
            Ordering::Greater => println!(
                "{}",
                Red.bold()
                    .paint("Too big! Try again")
            ),
            Ordering::Equal => {
                println!("{}", Green.bold().paint("You won! Yay 😃 👍!"));
                break;
            }
        }
        println!();
    }
    crate::end();
}
