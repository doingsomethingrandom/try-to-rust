use ansi_term::Colour::Red;

pub fn run(game: &str) {
    if game != "" {
        println!(
            "{}",
            Red.bold().paint(format!("UNKNOWN GAME: \"{}\"!", game))
        );
    }
    crate::title("HELP");
    println!("USAGE: rustygames <game>");
    println!();
    println!("GAMES:");
    println!("  help      displays helpful info");
    println!("  guess     a guessing game");
    println!("  fizzbuzz  a game which gets factors");
    crate::end();
}
