use ansi_term::Colour::{Blue, Cyan, Red, Yellow};
use inquire::Text;

pub fn run() {
    crate::title("FIZZBUZZ");
    let fizz = match Text::new("What shall fizz be?").prompt() {
        Ok(str) => str,
        Err(_) => panic!("{}", Red.blink().paint("Could not read input!"))
    };
    let fizz: u32 = match fizz.trim().parse() {
        Ok(num) => num,
        Err(_) => panic!("{}", Red.blink().paint("You must in put a number!")),
    };

    let buzz = match Text::new("What shall buzz be?").prompt() {
        Ok(str) => str,
        Err(_) => panic!("{}", Red.blink().paint("Could not read input!"))
    };
    let buzz: u32 = match buzz.trim().parse() {
        Ok(num) => num,
        Err(_) => panic!("{}", Red.blink().paint("You must in put a number!")),
    };

    let ammount = match Text::new("Enter the number you want to stop at:").prompt() {
        Ok(str) => str,
        Err(_) => panic!("{}", Red.blink().paint("Could not read input!"))
    };
    let ammount: u32 = match ammount.trim().parse() {
        Ok(num) => num,
        Err(_) => panic!("{}", Red.blink().paint("You must in put a number!")),
    };

    println!();

    for i in 0..=ammount {
        match (i % fizz, i % buzz) {
            (0, 0) => println!("{}", paint_fizzbuzz(i)),
            (0, _) => println!("{}", paint_fizz(i)),
            (_, 0) => println!("{}", paint_buzz(i)),
            (_, _) => println!("{}", paint_int(i.to_string().as_str())),
        }
    }
    
    crate::end();
}

fn paint_fizzbuzz(i: u32) -> String {
    return format!(
        "{}{} {}",
        Cyan.italic().blink().paint("Fizz"),
        Blue.italic().blink().paint("Buzz"),
        paint_int(format!("({})", i).as_str())
    );
}

fn paint_fizz(i: u32) -> String {
    return format!(
        "{} {}",
        Cyan.italic().paint("Fizz"),
        paint_int(format!("({})", i).as_str())
    );
}

fn paint_buzz(i: u32) -> String {
    return format!(
        "{} {}",
        Blue.italic().paint("Buzz"),
        paint_int(format!("({})", i).as_str())
    );
}

fn paint_int(i: &str) -> String {
    return format!("{}", Yellow.bold().paint(i));
}
