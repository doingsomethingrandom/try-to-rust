use ansi_term::Colour::Red;
use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();
    if &args.len() == &2 {
        rustygames::run(&args[1]);
    } else {
        println!(
            "{}",
            Red.blink().bold().paint(format!(
                "RUSTYGAMES ONLY SUPPORTS 1 ARGUMENT BUT IT GOT {}!",
                &args.len() - 1
            ))
        );
        rustygames::games::help::run("");
    }
}
